DECORATOR: Também conhecido como WRAPPER, é um padrão de projeto de software que permite adicionar um comportamento a um objeto já existente em tempo de execução, ou seja, agrega dinamicamente responsabilidades adicionais a um objeto

Para testar o padrão, execute a classe TesteDeImpostosComplexos.
